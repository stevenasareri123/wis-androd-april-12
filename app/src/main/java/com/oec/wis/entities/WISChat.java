package com.oec.wis.entities;

public class WISChat {
    String thumb;
    private int id;
    private WISUser user;
    private String msg;
    private String dateTime;
    private boolean isMe;
    private String typeMsg;
    private String amis_id;
    private int groupId;
    private String groupName;
    private String groupIcon;
    private int members;

    private String currentchannel;

    public String getAmis_id() {
        return amis_id;
    }

    public void setAmis_id(String amis_id) {
        this.amis_id = amis_id;
    }

    public String getCurrentchannel() {
        return currentchannel;
    }

    public void setCurrentchannel(String currentchannel) {
        this.currentchannel = currentchannel;
    }


    public WISChat(int id, WISUser user, String msg, String dateTime, boolean isMe, String typeMsg, String thumb, String amis_id) {
        this.id = id;
        this.user = user;
        this.msg = msg;
        this.dateTime = dateTime;
        this.isMe = isMe;
        this.typeMsg = typeMsg;
        this.thumb = thumb;

        this.amis_id = amis_id;



    }

    public WISChat(int id, WISUser user, String msg, String dateTime, boolean isMe, String typeMsg, String thumb, String amis_id, String currentchannel) {
        this.id = id;
        this.user = user;
        this.msg = msg;
        this.dateTime = dateTime;
        this.isMe = isMe;
        this.typeMsg = typeMsg;
        this.thumb = thumb;

        this.amis_id = amis_id;


        this.currentchannel = currentchannel;
    }

    public void setMe(boolean me) {
        isMe = me;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public WISChat(boolean isMe,int groupId, String groupName, String groupIcon, int members,String lastmessage,String created_at,String currentchannel){

        this.isMe = isMe;
        this.groupId  =groupId;
        this.groupName = groupName;
        this.msg = lastmessage;
        this.members = members;
        this.groupIcon = groupIcon;
        this.dateTime = created_at;

        this.currentchannel = currentchannel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WISUser getUser() {
        return user;
    }

    public void setUser(WISUser user) {
        this.user = user;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String created_at) {
        this.dateTime = created_at;
    }

    public boolean getIsMe() {
        return isMe;
    }

    public void setIsMe(boolean isMe) {
        this.isMe = isMe;
    }

    public boolean isMe() {
        return isMe;
    }

    public String getTypeMsg() {
        return typeMsg;
    }

    public void setTypeMsg(String typeMsg) {
        this.typeMsg = typeMsg;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
