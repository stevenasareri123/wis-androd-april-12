package com.oec.wis.entities;

/**
 * Created by asareri08 on 21/07/16.
 */
public class EntryItem implements Item{
    private int id;
    private String firstName;
    private String lastName;
    private String pic;
    private int nFriend;
    private String pType;
    private String email;
    private String sexe;
    private int nCFriend;
    private int lInvit;
    private String fullName;
    private String objectId;
    private String unfollowerId;
    private Boolean headerSection;

    private WISUser user;

    public WISUser getUser() {
        return user;
    }

    public void setUser(WISUser user) {
        this.user = user;
    }

    private  String publishNewsFeed;

    private Boolean isGroupChatalive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getnFriend() {
        return nFriend;
    }

    public void setnFriend(int nFriend) {
        this.nFriend = nFriend;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public int getnCFriend() {
        return nCFriend;
    }

    public void setnCFriend(int nCFriend) {
        this.nCFriend = nCFriend;
    }

    public int getlInvit() {
        return lInvit;
    }

    public void setlInvit(int lInvit) {
        this.lInvit = lInvit;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getUnfollowerId() {
        return unfollowerId;
    }

    public void setUnfollowerId(String unfollowerId) {
        this.unfollowerId = unfollowerId;
    }

    public String getPublishNewsFeed() {
        return publishNewsFeed;
    }

    public void setPublishNewsFeed(String publishNewsFeed) {
        this.publishNewsFeed = publishNewsFeed;
    }

    public Boolean getGroupChatalive() {
        return isGroupChatalive;
    }

    public void setGroupChatalive(Boolean groupChatalive) {
        isGroupChatalive = groupChatalive;
    }

    public EntryItem(Boolean isGroupChatAlive, String unfollowerId, String objectId, String publishNewsFeed, int id, String fullName, String pic, int nFriend) {
        this.id = id;

        this.fullName = fullName;
        this.publishNewsFeed = publishNewsFeed;
        this.objectId = objectId;

        this.pic = pic;

        this.nFriend = nFriend;
        this.unfollowerId = unfollowerId;
    }

    public EntryItem(WISUser user){

        this.user = user;


    }

    @Override
    public boolean isSection() {
        return false;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getfullName() {
        return null;
    }
}
