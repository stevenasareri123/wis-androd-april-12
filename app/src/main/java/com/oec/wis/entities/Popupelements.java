package com.oec.wis.entities;

import android.widget.ImageView;

/**
 * Created by asareri08 on 02/06/16.
 */
public class Popupelements {
    String title;
    int id;

    public Popupelements(int id,String title){
        this.id = id;
        this.title=title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
