package com.oec.wis.entities;

import java.util.List;

/**
 * Created by asareri12 on 22/11/16.
 */


public class WISPlaylistSongs {


    private int id;
    private String created_at;



    private String created_by;
    private String type;
    private String name;

    private List<WISPlaylist> listdata;

    private List<WISMusic> songsList;

    public WISPlaylistSongs(){}

    public WISPlaylistSongs(int id, String type, String created_at, String created_by, String name, List<WISPlaylist> listdata) {
        this.id = id;
        this.created_at = created_at;
        this.created_by=created_by;
        this.name = name;
        this.type = type;
        this.listdata=listdata;
    }
    public WISPlaylistSongs(int id, String type, String created_at, String created_by, String name) {
        this.id = id;
        this.created_at = created_at;
        this.created_by=created_by;
        this.name = name;
        this.type = type;
    }
    public WISPlaylistSongs(int id, String created_at, String name) {
        this.id = id;
        this.created_at = created_at;
        this.name = name;
    }

    public List<WISMusic> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<WISMusic> songsList) {
        this.songsList = songsList;
    }

    public WISPlaylistSongs(List<WISMusic> songsdata, int id, String type, String created_at, String created_by, String name) {
        this.id = id;
        this.created_at = created_at;
        this.created_by=created_by;
        this.name = name;

        this.type = type;
        this.songsList=songsdata;
    }
    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WISPlaylist> getListdata() {
        return listdata;
    }

    public void setListdata(List<WISPlaylist> listdata) {
        this.listdata = listdata;
    }


}

