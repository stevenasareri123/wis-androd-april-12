package com.oec.wis.entities;

public enum WISLanguage {
    fr_FR,
    en_US,
    es_ES
}
