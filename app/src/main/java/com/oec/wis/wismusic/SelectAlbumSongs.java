package com.oec.wis.wismusic;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.oec.wis.R;

/**
 * Created by asareri12 on 07/12/16.
 */

public class SelectAlbumSongs extends ListActivity {

    Cursor cursor;

    String ALBUM_ID;

    String albumid;

    ListView listView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_music);

        listView = (ListView) findViewById(R.id.list);

        Intent intent = getIntent();
        ALBUM_ID = intent.getStringExtra("album_id");

        String[] column = { MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.MIME_TYPE, };

        String where = android.provider.MediaStore.Audio.Media.ALBUM + "=?";

        String whereVal[] = { "Intinta Annamayya - (2013)" };

        String orderBy = android.provider.MediaStore.Audio.Media.TITLE;

        cursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                column, where, whereVal, orderBy);

        if (cursor.moveToFirst()) {
            do {
                Log.v("Vipul",
                        cursor.getString(cursor
                                .getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
            } while (cursor.moveToNext());
        }

        String[] dispField = new String[] {MediaStore.Audio.Media.TITLE};
        int[] displayView = new int[] {android.R.id.text1};
        setListAdapter(new SimpleCursorAdapter(this,android.R.layout.simple_list_item_1,cursor,dispField,displayView));
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Toast.makeText(getApplicationContext(),"You clicked at position "+ String.valueOf(position)+":"+ String.valueOf(id), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this,SelectAlbumSongs.class);
        i.putExtra("album_id",id);
        startActivity(i);
    }
}
