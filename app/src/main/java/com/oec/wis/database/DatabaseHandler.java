package com.oec.wis.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISAlbumPlaylist;
import com.oec.wis.entities.WISMusic;
import com.oec.wis.entities.WISPlaylist;
import com.oec.wis.entities.WISPlaylist1;
import com.oec.wis.entities.WISPlaylistAlbum;
import com.oec.wis.entities.WISPlaylistSongs;
import com.oec.wis.entities.WISSongs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asareri12 on 17/01/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "musicManager";

    // ALBUM_FILES_TABLE name
    private static final String ALBUM_FILES_TABLE = "album_files";

    // ALBUM_FILES Table Columns names
    private static final String KEY_ALBUM_NAME = "album_name";
    private static final String KEY_ALBUM_ARTISTS = "album_artists";
    private static final String KEY_ALBUM_CREATED_AT = "album_created_at";


    // SONGS_FILES_TABLE table name
    private static final String SONGS_FILES_TABLE = "songs_files";

    // SONGS_FILES_TABLE Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_ARTISTS = "artists";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_PATH = "audio_path";
    private static final String KEY_THUMB = "audio_thumbnail";
    private static final String KEY_CREATED_AT = "created_at";

    // SONGS_TABLE table name

    private static final String SONGS_TABLE = "songs";


    // SONGS_TABLE Columns name
    private static final String KEY_SONG_ID = "song_id";
    private static final String KEY_ALBUM_ID = "album_id";
    private static final String KEY_PLAYLIST_ID = "playlist_id";

    // PLAYLIST_SONGS_FILES table

    private static final String PLAYLIST_SONGS_FILES_TABLE = "playlist_songs_files";
    private static final String PLAYLIST_ALBUM_FILES_TABLE = "playlist_album_files";
    private static final String PLAYLIST_ALBUMS = "playlist_all_albums";

    // PLAYLIST_SONGS_FILES Table Columns names
    private static final String KEY_TYPE = "type";
    private static final String KEY_CREATED_BY = "created_by";

    private static final String PLAYLIST_SONGS_TABLE = "playlist_songs";
    private static final String PLAYLIST_ALBUM_TABLE = "playlist_album_songs";
    private static final String KEY_THUMBNAIL = "thumbnail";
    private static final String KEY_TITLE = "title";


    // album_files table create statement

    private static final String CREATE_ALBUM_FILES_TABLE = "CREATE TABLE "
            + ALBUM_FILES_TABLE + "(" + KEY_ALBUM_ID + " INTEGER," + KEY_ALBUM_NAME
            + " TEXT," + KEY_ALBUM_ARTISTS + " TEXT," + KEY_ALBUM_CREATED_AT
            + " TEXT" + ")";

    // songs_files table create statement
    private static final String CREATE_SONGS_FILES_TABLE = "CREATE TABLE " + SONGS_FILES_TABLE + "("
            + KEY_SONG_ID + " INTEGER," + KEY_NAME + " TEXT,"
            + KEY_ARTISTS + " TEXT," + KEY_DURATION + " TEXT," + KEY_PATH + " TEXT," + KEY_THUMB + " TEXT," + KEY_CREATED_AT + " TEXT" +")";

    //songs table create statement
    private static final String CREATE_SONGS_TABLE = "CREATE TABLE " + SONGS_TABLE + "("
            + KEY_SONG_ID + " INTEGER,"+ KEY_ALBUM_ID + " INTEGER," + KEY_NAME + " TEXT,"
            + KEY_ARTISTS + " TEXT," + KEY_DURATION + " TEXT," + KEY_PATH + " TEXT," + KEY_THUMB + " TEXT," + KEY_CREATED_AT + " TEXT" +")";

    // playlist_songs_files table create statement
    private static final String CREATE_PLAYLIST_SONGS_FILES_TABLE = "CREATE TABLE " + PLAYLIST_SONGS_FILES_TABLE + "("
            + KEY_ID + " INTEGER,"+ KEY_NAME + " TEXT," + KEY_TYPE + " TEXT,"+ KEY_CREATED_AT + " TEXT" +")";
    //
    // playlist_songs table create statement
    private static final String CREATE_PLAYLIST_SONGS_TABLE = "CREATE TABLE " + PLAYLIST_SONGS_TABLE + "("
            + KEY_ID + " INTEGER,"+ KEY_ALBUM_ID + " INTEGER," + KEY_THUMBNAIL + " TEXT,"+ KEY_TITLE + " TEXT,"
            + KEY_ARTISTS + " TEXT," + KEY_CREATED_AT + " TEXT," + KEY_PATH + " TEXT," + KEY_DURATION + " TEXT," + KEY_TYPE + " TEXT" +")";

    private static final String CREATE_PLAYLIST_ALBUM_FILES_TABLE = "CREATE TABLE " + PLAYLIST_ALBUM_FILES_TABLE + "("
            + KEY_PLAYLIST_ID + " INTEGER,"+ KEY_NAME + " TEXT," + KEY_TYPE + " TEXT,"+ KEY_CREATED_AT + " TEXT" +")";

    private static final String CREATE_PLAYLIST_ALBUMS = "CREATE TABLE " + PLAYLIST_ALBUMS + "("
            + KEY_ALBUM_ID + " INTEGER,"+ KEY_PLAYLIST_ID + " INTEGER,"+ KEY_ALBUM_NAME + " TEXT," + KEY_ALBUM_ARTISTS + " TEXT,"+ KEY_CREATED_AT + " TEXT" +")";

    private static final String CREATE_PLAYLIST_ALBUM_TABLE = "CREATE TABLE " + PLAYLIST_ALBUM_TABLE + "("
            + KEY_ID + " INTEGER,"+ KEY_ALBUM_ID + " INTEGER," + KEY_THUMBNAIL + " TEXT,"+ KEY_TITLE + " TEXT,"
            + KEY_ARTISTS + " TEXT," + KEY_CREATED_AT + " TEXT," + KEY_PATH + " TEXT," + KEY_DURATION + " TEXT," + KEY_TYPE + " TEXT" +")";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_ALBUM_FILES_TABLE);
        db.execSQL(CREATE_SONGS_TABLE);

        db.execSQL(CREATE_SONGS_FILES_TABLE);


        db.execSQL(CREATE_PLAYLIST_SONGS_FILES_TABLE);
        db.execSQL(CREATE_PLAYLIST_SONGS_TABLE);

        db.execSQL(CREATE_PLAYLIST_ALBUM_FILES_TABLE);
        db.execSQL(CREATE_PLAYLIST_ALBUMS);
        db.execSQL(CREATE_PLAYLIST_ALBUM_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + ALBUM_FILES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SONGS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SONGS_FILES_TABLE);


        // playlist data

        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_SONGS_FILES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_SONGS_TABLE);

        // PLAYLIST ALBUM DATA

        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_ALBUM_FILES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_ALBUMS);
        db.execSQL("DROP TABLE IF EXISTS" + PLAYLIST_ALBUM_TABLE);

        // Create tables again
        onCreate(db);
    }
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */


    public void addAlbums(WISAlbum wisAlbum) {
        //for logging
        Log.d("insertSongs", wisAlbum.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ALBUM_ID, wisAlbum.getId());
        values.put(KEY_ALBUM_NAME, wisAlbum.getName());
        values.put(KEY_ALBUM_ARTISTS, wisAlbum.getArtists());
        values.put(KEY_ALBUM_CREATED_AT, wisAlbum.getDateTime());
        Cursor cursor = null;
        String sql = "SELECT  * FROM " + ALBUM_FILES_TABLE + " WHERE "
                + KEY_ALBUM_ID + " = " + wisAlbum.getId();
        cursor= db.rawQuery(sql,null);


        if(cursor.getCount()>0){
            //PID Found
            Log.d("notInsert", String.valueOf(wisAlbum.getId()));
        }else{
            db.insert(ALBUM_FILES_TABLE, null, values);
        }

        db.close(); // Closing database connection
    }
    public void putSongs(WISSongs wisSongs) {

        //for logging
        Log.d("insertSongs", wisSongs.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_SONG_ID, wisSongs.getId());
        values.put(KEY_ALBUM_ID,wisSongs.getAlbum_id());
        values.put(KEY_NAME, wisSongs.getName());
        values.put(KEY_ARTISTS, wisSongs.getA());
        values.put(KEY_DURATION, wisSongs.getDuration());
        values.put(KEY_PATH, wisSongs.getPath());
        values.put(KEY_THUMB, wisSongs.getThumb());
        values.put(KEY_CREATED_AT, wisSongs.getCreated_at());

        Cursor cursor = null;
        String sql = "SELECT  * FROM " + SONGS_TABLE + " WHERE "
                + KEY_SONG_ID + " = " + wisSongs.getId();
        cursor= db.rawQuery(sql,null);


        if(cursor.getCount()>0){
            //PID Found
            Log.d("notInsert", String.valueOf(wisSongs.getId()));
        }else{
            db.insert(SONGS_TABLE, null, values);
        }

        // 4. close
        db.close(); // Closing database connection
    }
    public void addSongs(WISMusic wisMusic) {

        //for logging
        Log.d("addSongs", wisMusic.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_SONG_ID, wisMusic.getId());
        values.put(KEY_NAME, wisMusic.getName());
        values.put(KEY_ARTISTS, wisMusic.getArtists());
        values.put(KEY_DURATION, wisMusic.getDuration());
        values.put(KEY_PATH, wisMusic.getPath());
        values.put(KEY_THUMB, wisMusic.getThumb());
        values.put(KEY_CREATED_AT, wisMusic.getDateTime());

        Cursor cursor = null;
        String sql = "SELECT  * FROM " + SONGS_FILES_TABLE + " WHERE "
                + KEY_SONG_ID + " = " + wisMusic.getId();
        cursor= db.rawQuery(sql,null);


        if(cursor.getCount()>0){
            //PID Found
            Log.d("notInsert", String.valueOf(wisMusic.getId()));
        }else{
            db.insert(SONGS_FILES_TABLE, null, values);
        }
        // 4. close
        db.close(); // Closing database connection
    }

    public void addPlaylistSongs(WISPlaylistSongs wisPlaylistSongs) {
        //for logging
        Log.d("addAlbums", wisPlaylistSongs.toString());
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, wisPlaylistSongs.getId());
        values.put(KEY_NAME, wisPlaylistSongs.getName());
        values.put(KEY_TYPE, wisPlaylistSongs.getType());
        values.put(KEY_CREATED_AT, wisPlaylistSongs.getCreated_at());
        Cursor cursor = null;
        String sql = "SELECT  * FROM " + PLAYLIST_SONGS_FILES_TABLE + " WHERE "
                + KEY_ID + " = " + wisPlaylistSongs.getId();
        cursor= db.rawQuery(sql,null);

        db.insert(PLAYLIST_SONGS_FILES_TABLE, null, values);
//        if(cursor.getCount()>0){
//            //PID Found
//            Log.d("notInsert",String.valueOf(wisPlaylistSongs.getId()));
//        }else{
//            db.insert(PLAYLIST_SONGS_FILES_TABLE, null, values);
//        }
        db.close(); // Closing database connection
    }

    public void putPlaylistSongs(WISPlaylist wisPlaylist) {

        //for logging
        Log.d("insertSongs", wisPlaylist.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_ID, wisPlaylist.getId());
        values.put(KEY_ALBUM_ID, wisPlaylist.getAlbum());
        values.put(KEY_THUMBNAIL, wisPlaylist.getThumbnail());
        values.put(KEY_TITLE, wisPlaylist.getName());
        values.put(KEY_ARTISTS, wisPlaylist.getArtists());
        values.put(KEY_CREATED_AT, wisPlaylist.getCreated_at());
        values.put(KEY_PATH, wisPlaylist.getAudio_path());
        values.put(KEY_DURATION, wisPlaylist.getDuration());
        values.put(KEY_TYPE, wisPlaylist.getType());

        Cursor cursor = null;
        String sql = "SELECT  * FROM " + PLAYLIST_SONGS_TABLE + " WHERE "
                + KEY_ID + " = " + wisPlaylist.getId();
        cursor= db.rawQuery(sql,null);
        db.insert(PLAYLIST_SONGS_TABLE, null, values);

//        if(cursor.getCount()>0){
//            //PID Found
//            Log.d("notInsert",String.valueOf(wisPlaylist.getId()));
//        }else{
//
//        }
        // 4. close
        db.close(); // Closing database connection
    }
    //
    public void addPlaylistAllAlbums(WISPlaylistAlbum wisPlaylistAlbums) {
        //for logging
        Log.d("addAlbums", wisPlaylistAlbums.toString());
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_ID, wisPlaylistAlbums.getId());
        values.put(KEY_NAME, wisPlaylistAlbums.getName());
        values.put(KEY_TYPE, wisPlaylistAlbums.getType());
        values.put(KEY_CREATED_AT, wisPlaylistAlbums.getCreated_at());

        Cursor cursor = null;
        String sql = "SELECT  * FROM " + PLAYLIST_ALBUM_FILES_TABLE + " WHERE "
                + KEY_PLAYLIST_ID + " = " + wisPlaylistAlbums.getId();
        cursor= db.rawQuery(sql,null);

        db.insert(PLAYLIST_ALBUM_FILES_TABLE, null, values);

//        if(cursor.getCount()>0){
//            //PID Found
//            Log.d("notInsert",String.valueOf(wisPlaylistAlbums.getId()));
//        }else{
//            db.insert(PLAYLIST_ALBUM_FILES_TABLE, null, values);
//        }
        db.close(); // Closing database connection
    }
    public void addPlaylistAddAlbums(WISAlbumPlaylist wisAlbumPlaylist) {

        System.out.println("Add Album to Playlist called");
        Log.e("Album ids:;", String.valueOf(wisAlbumPlaylist.getAlbum_id()));
        Log.e("Play ids:", String.valueOf(wisAlbumPlaylist.getPlaylist_id()));

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ALBUM_ID, wisAlbumPlaylist.getAlbum_id());
        values.put(KEY_PLAYLIST_ID, wisAlbumPlaylist.getPlaylist_id());
        values.put(KEY_ALBUM_NAME, wisAlbumPlaylist.getAlbum_name());
        values.put(KEY_ALBUM_ARTISTS, wisAlbumPlaylist.getAlbum_artists());
        values.put(KEY_CREATED_AT, wisAlbumPlaylist.getCreated_at());
        Cursor cursor = null;
        String sql = "SELECT  * FROM " + PLAYLIST_ALBUMS + " WHERE "
                + KEY_ALBUM_ID + " = " + wisAlbumPlaylist.getAlbum_id();
        cursor= db.rawQuery(sql,null);



        db.insert(PLAYLIST_ALBUMS, null, values);

//        if(cursor.getCount()>0){
//            //PID Found
//            Log.d("notInsert",String.valueOf(wisAlbumPlaylist.getAlbum_id()));
//        }else{
//            db.insert(PLAYLIST_ALBUMS, null, values);
//        }
        db.close(); // Closing database connection
    }
    public void putPlaylistAlbumSongs(WISPlaylist1 wisPlaylist) {

        //for logging
        Log.d("insertSongs", wisPlaylist.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_ID, wisPlaylist.getId());
        values.put(KEY_ALBUM_ID, wisPlaylist.getAlbum_id());
        values.put(KEY_THUMBNAIL, wisPlaylist.getThumbnail());
        values.put(KEY_TITLE, wisPlaylist.getName());
        values.put(KEY_ARTISTS, wisPlaylist.getArtists());
        values.put(KEY_CREATED_AT, wisPlaylist.getCreated_at());
        values.put(KEY_PATH, wisPlaylist.getAudio_path());
        values.put(KEY_DURATION, wisPlaylist.getDuration());
        values.put(KEY_TYPE, wisPlaylist.getType());

        Cursor cursor = null;
        String sql = "SELECT  * FROM " + PLAYLIST_ALBUM_TABLE + " WHERE "
                + KEY_ID + " = " + wisPlaylist.getId();
        cursor= db.rawQuery(sql,null);

        db.insert(PLAYLIST_ALBUM_TABLE, null, values);

//        if(cursor.getCount()>0){
//            //PID Found
//            Log.d("notInsert",String.valueOf(wisPlaylist.getId()));
//        }else{
//            db.insert(PLAYLIST_ALBUM_TABLE, null, values);
//        }
        // 4. close
        db.close(); // Closing database connection
    }




    // Getting getAllAlbums
    public List<WISAlbum> getAllAlbums() {
        List<WISAlbum> allAlbums = new ArrayList<WISAlbum>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ALBUM_FILES_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        WISAlbum wisAlbum = null;
        if (cursor.moveToFirst()) {
            do {
                wisAlbum = new WISAlbum();
                wisAlbum.setId(Integer.parseInt(cursor.getString(0)));
                wisAlbum.setName(cursor.getString(1));
                wisAlbum.setArtists(cursor.getString(2));
                wisAlbum.setDateTime(cursor.getString(3));

                // Adding contact to list
                allAlbums.add(wisAlbum);
            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }
    // Getting getSongs
    public List<WISSongs> getAlbumSongs(int albumID) {
        List<WISSongs> getSongs = new ArrayList<WISSongs>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + SONGS_TABLE + " WHERE "
                + KEY_ALBUM_ID + " = " + albumID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        WISSongs wisSongs = null;
        if (cursor.moveToFirst()) {
            do {
                wisSongs = new WISSongs();
                wisSongs.setId(Integer.parseInt(cursor.getString(0)));
                wisSongs.setAlbum_id(Integer.parseInt(cursor.getString(1)));
                wisSongs.setName(cursor.getString(2));
                wisSongs.setA(cursor.getString(3));
                wisSongs.setDuration(cursor.getString(4));
                wisSongs.setPath(cursor.getString(5));
                wisSongs.setThumb(cursor.getString(6));
                wisSongs.setCreated_at(cursor.getString(7));
                // Adding contact to list
                getSongs.add(wisSongs);
            } while (cursor.moveToNext());
        }
        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }
    // Getting getAllSongs
    public List<WISMusic> getAllSongs() {
        List<WISMusic> allsongs = new ArrayList<WISMusic>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + SONGS_FILES_TABLE;
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        WISMusic wisMusic = null;
        if (cursor.moveToFirst()) {
            do {
                wisMusic = new WISMusic();
                wisMusic.setId(Integer.parseInt(cursor.getString(0)));
                wisMusic.setName(cursor.getString(1));
                wisMusic.setArtists(cursor.getString(2));
                wisMusic.setDuration(cursor.getString(3));
                wisMusic.setPath(cursor.getString(4));
                wisMusic.setThumb(cursor.getString(5));
                wisMusic.setDateTime(cursor.getString(6));

                // Adding contact to list
                allsongs.add(wisMusic);
            } while (cursor.moveToNext());
        }
        Log.d("getAllSongs()", allsongs.toString());

        // return contact list
        return allsongs;
    }

    // Getting getAllAlbums
    public List<WISPlaylistSongs> getAllPlaylistSongs() {
        List<WISPlaylistSongs> allAlbums = new ArrayList<WISPlaylistSongs>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_SONGS_FILES_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        WISPlaylistSongs wisPlaylistSongs = null;
        if (cursor.moveToFirst()) {
            do {
                wisPlaylistSongs = new WISPlaylistSongs();
                wisPlaylistSongs.setId(Integer.parseInt(cursor.getString(0)));
                wisPlaylistSongs.setName(cursor.getString(1));
                wisPlaylistSongs.setType(cursor.getString(2));
                wisPlaylistSongs.setCreated_at(cursor.getString(3));

                // Adding contact to list



//                get song id from playlist

                Log.e("sonplaylist id::",cursor.getString(0));

                List<WISMusic> song_list = getSongsByPlaylistId(Integer.parseInt(cursor.getString(0)));

                Log.e("song_list::", String.valueOf(song_list));

                wisPlaylistSongs.setSongsList(song_list);

                allAlbums.add(wisPlaylistSongs);

            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }

    public List<WISMusic> getSongsByPlaylistId(int albumID) {

        System.out.print("+++++ getSongsByPlaylistId +++++");

//        Log.e("All Playlist Songs::",getAllPlaylistSongs());

        for (WISPlaylist songs:fetchPlayListSongs()
                ) {

            Log.e("Album Ids::", String.valueOf(songs.getAlbum()));


        }


        List<WISMusic> getSongs = new ArrayList<>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_SONGS_TABLE + " WHERE " + KEY_ALBUM_ID + " = " + albumID;
        Log.i("songQuery",selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list

        System.out.println("WisSongsbyplaylist ids::");

        Log.e("Curso status", String.valueOf(cursor.moveToFirst()));


        try {
            WISMusic wisPlaylist = null;
            if (cursor.moveToFirst()) {
                do {
                    wisPlaylist = new WISMusic();
                    wisPlaylist.setId(Integer.parseInt(cursor.getString(0)));
                    wisPlaylist.setThumb(cursor.getString(2));
                    wisPlaylist.setName(cursor.getString(3));
                    wisPlaylist.setArtists(cursor.getString(4));
                    wisPlaylist.setDateTime(cursor.getString(5));
                    wisPlaylist.setPath(cursor.getString(6));
                    wisPlaylist.setDuration(cursor.getString(7));

                    // Adding contact to list
                    getSongs.add(wisPlaylist);
                } while (cursor.moveToNext());
            }
        }
        catch (IndexOutOfBoundsException e)
        {

        }

        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }



    //
//    // Getting getSongs
//
//
    public List<WISPlaylist> getPlaylistSongs(int albumID) {
        List<WISPlaylist> getSongs = new ArrayList<WISPlaylist>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_SONGS_TABLE + " WHERE " + KEY_ALBUM_ID + " = " + albumID;
        Log.i("songQuery",selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        try {
            WISPlaylist wisPlaylist = null;
            if (cursor.moveToFirst()) {
                do {
                    wisPlaylist = new WISPlaylist();
                    wisPlaylist.setId(Integer.parseInt(cursor.getString(0)));
                    wisPlaylist.setAlbum(Integer.parseInt(cursor.getString(1)));
                    wisPlaylist.setThumbnail(cursor.getString(2));
                    wisPlaylist.setName(cursor.getString(3));
                    wisPlaylist.setArtists(cursor.getString(4));
                    wisPlaylist.setCreated_at(cursor.getString(5));
                    wisPlaylist.setAudio_path(cursor.getString(6));
                    wisPlaylist.setDuration(cursor.getString(7));
                    wisPlaylist.setType(cursor.getString(8));
                    // Adding contact to list
                    getSongs.add(wisPlaylist);
                } while (cursor.moveToNext());
            }
        }
        catch (IndexOutOfBoundsException e)
        {

        }

        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }


    public List<WISPlaylist> fetchPlayListSongs() {
        List<WISPlaylist> getSongs = new ArrayList<WISPlaylist>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_SONGS_TABLE;
//                + " WHERE "
//                + KEY_ALBUM_ID + " = " + song_id;
        // 2. get reference to writable DB

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        WISPlaylist wisPlaylist = null;
        if (cursor.moveToFirst()) {
            do {

                wisPlaylist = new WISPlaylist();
                wisPlaylist.setId(Integer.parseInt(cursor.getString(0)));
                wisPlaylist.setAlbum(Integer.parseInt(cursor.getString(1)));
                wisPlaylist.setThumbnail(cursor.getString(2));
                wisPlaylist.setName(cursor.getString(3));
                wisPlaylist.setArtists(cursor.getString(4));
                wisPlaylist.setCreated_at(cursor.getString(5));
                wisPlaylist.setAudio_path(cursor.getString(6));
                wisPlaylist.setDuration(cursor.getString(7));
                wisPlaylist.setType(cursor.getString(8));


                // Adding contact to list
                getSongs.add(wisPlaylist);
            } while (cursor.moveToNext());
        }
        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }

    public List<WISPlaylistAlbum> getAllPlaylistAlbums() {

        System.out.println("get All Playlist Caled");

        List<WISPlaylistAlbum> allAlbums = new ArrayList<WISPlaylistAlbum>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUM_FILES_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        WISPlaylistAlbum wisPlaylistSongs = null;

        Log.e("Playlist Cursor", String.valueOf(cursor.moveToFirst()));

        if (cursor.moveToFirst()) {
            do {
//                wisPlaylistSongs = new WISPlaylistAlbum();
//                wisPlaylistSongs.setId(Integer.parseInt(cursor.getString(0)));
//                wisPlaylistSongs.setName(cursor.getString(1));
//                wisPlaylistSongs.setType(cursor.getString(2));
//                wisPlaylistSongs.setCreated_at(cursor.getString(3));
//                // Adding contact to list
//                allAlbums.add(wisPlaylistSongs);


                Log.e("Playlist ID =>",cursor.getString(0));

                List<WISAlbum> albumData = getAlbumsByPlaylistId(Integer.parseInt(cursor.getString(0)));
                allAlbums.add(new WISPlaylistAlbum(albumData, Integer.parseInt(cursor.getString(0)),"album",cursor.getString(3),"0",cursor.getString(1)));

            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }




    public List<WISAlbum> getAlbumsByPlaylistId(int playlistId){

        System.out.println("+++++++getAlbumsByPlaylistIdcalled+++++++");

        for (WISAlbumPlaylist albumPlay:testPlaylistAlbum()
                ) {


            Log.e("Play list ids", String.valueOf(albumPlay.getPlaylist_id()));

            Log.e("Album list ids", String.valueOf(albumPlay.getAlbum_id()));
        }


        Log.e("All album Play:", String.valueOf(testPlaylistAlbum()));


        Log.e("get all playalbum", String.valueOf(getAllPlaylistAddAlbums(playlistId)));



        List<WISAlbum> albums =  new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS + " WHERE "
                + KEY_PLAYLIST_ID + " = " + playlistId;

        Log.e("Album Query==>",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.e("PlaylistId custaru==>", String.valueOf(cursor.moveToFirst()));

        // looping through all rows and adding to list
        WISAlbumPlaylist wisPlaylistSongs = null;
        if (cursor.moveToFirst()) {
            do {
//                wisPlaylistSongs = new WISAlbumPlaylist();
//                wisPlaylistSongs.setAlbum_id(Integer.parseInt(cursor.getString(0)));
//                wisPlaylistSongs.setPlaylist_id(Integer.parseInt(cursor.getString(1)));
//                wisPlaylistSongs.setAlbum_name(cursor.getString(2));
//                wisPlaylistSongs.setAlbum_artists(cursor.getString(3));
//                wisPlaylistSongs.setCreated_at(cursor.getString(4));
//
//                // Adding contact to list
//                albums.add(wisPlaylistSongs);

//                get songs by album id


                System.out.println("Album::");

                Log.e("get Album Sons:;", String.valueOf(getAlbumSongs(Integer.parseInt(cursor.getString(0)))));

                List<WISSongs> song_set  = getAlbumSongs(Integer.parseInt(cursor.getString(0)));

                Log.e("Song-files::", String.valueOf(song_set));


//                List<WISPlaylist1>song_files = getPlaylistAlbumsByAlbumId(Integer.parseInt(cursor.getString(0)));

                albums.add(new WISAlbum(cursor.getString(2), Integer.parseInt(cursor.getString(0)),cursor.getString(3),cursor.getString(4),song_set));



//                albums.add(new WISAlbumPlaylist(song_set,cursor.getString(2),Integer.parseInt(cursor.getString(0)),cursor.getString(3),cursor.getString(4)));



            } while (cursor.moveToNext());
        }


        System.out.println("________________________");

        Log.e("Album SEt::", String.valueOf(albums));

        return albums;

    }

    public List<WISAlbumPlaylist> testPlaylistAlbum() {
        List<WISAlbumPlaylist> allAlbums = new ArrayList<WISAlbumPlaylist>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS;

        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS;

        Log.e("Query::",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        WISAlbumPlaylist wisPlaylistSongs = null;
        if (cursor.moveToFirst()) {
            do {
                wisPlaylistSongs = new WISAlbumPlaylist();
                wisPlaylistSongs.setAlbum_id(Integer.parseInt(cursor.getString(0)));
                wisPlaylistSongs.setPlaylist_id(Integer.parseInt(cursor.getString(1)));
                wisPlaylistSongs.setAlbum_name(cursor.getString(2));
                wisPlaylistSongs.setAlbum_artists(cursor.getString(3));
                wisPlaylistSongs.setCreated_at(cursor.getString(4));

                // Adding contact to list
                allAlbums.add(wisPlaylistSongs);
            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }
    public List<WISAlbumPlaylist> getAllPlaylistAddAlbums(int playlistId ) {
        List<WISAlbumPlaylist> allAlbums = new ArrayList<WISAlbumPlaylist>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS;

        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS + " WHERE "
                + KEY_PLAYLIST_ID + " = " + playlistId;

        Log.e("Query::",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        WISAlbumPlaylist wisPlaylistSongs = null;
        if (cursor.moveToFirst()) {
            do {
                wisPlaylistSongs = new WISAlbumPlaylist();
                wisPlaylistSongs.setAlbum_id(Integer.parseInt(cursor.getString(0)));
                wisPlaylistSongs.setPlaylist_id(Integer.parseInt(cursor.getString(1)));
                wisPlaylistSongs.setAlbum_name(cursor.getString(2));
                wisPlaylistSongs.setAlbum_artists(cursor.getString(3));
                wisPlaylistSongs.setCreated_at(cursor.getString(4));

                // Adding contact to list
                allAlbums.add(wisPlaylistSongs);
            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }

    public List<WISAlbumPlaylist> getPlaylistForAlbum(int playlistId) {
        List<WISAlbumPlaylist> allAlbums = new ArrayList<WISAlbumPlaylist>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS;

        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUMS + " WHERE "
                + KEY_PLAYLIST_ID + " = " + playlistId;



        Log.e("PlaylistAlbum Query:",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.e("Cursor list", String.valueOf(cursor));

        System.out.println(" PLAYLIST_ALBUMS Details");


        Log.e("cursor Status", String.valueOf(cursor.moveToFirst()));

//        for (WISAlbumPlaylist albumplaylist:
//             ) {
//
//        }

        // looping through all rows and adding to list
        WISAlbumPlaylist wisPlaylistSongs = null;
        if (cursor.moveToFirst()) {

            Log.e("Cursor","called");

            do {
                wisPlaylistSongs = new WISAlbumPlaylist();
                wisPlaylistSongs.setAlbum_id(Integer.parseInt(cursor.getString(0)));
                wisPlaylistSongs.setPlaylist_id(Integer.parseInt(cursor.getString(1)));
                wisPlaylistSongs.setAlbum_name(cursor.getString(2));
                wisPlaylistSongs.setAlbum_artists(cursor.getString(3));
                wisPlaylistSongs.setCreated_at(cursor.getString(4));

                // Adding contact to list
                //allAlbums.add(wisPlaylistSongs);
                //String album_name,int album_id,String album_artists,String created_at,List<WISPlaylist1> listdata

                System.out.println("get Album by id");

                Log.e("Album id",cursor.getString(0));


                List<WISPlaylist1> song_files = getPlaylistAlbumsByAlbumId(Integer.parseInt(cursor.getString(0)));

                Log.e("Song-files::", String.valueOf(song_files));

//                allAlbums.add(new WISAlbumPlaylist(cursor.getString(2),Integer.parseInt(cursor.getString(0)),cursor.getString(3),cursor.getString(4),song_files));
            } while (cursor.moveToNext());
        }

        // return contact list
        return allAlbums;
    }

    public List<WISPlaylist1> getPlaylistAlbumsByAlbumId(int album_id) {
        List<WISPlaylist1> getSongs = new ArrayList<WISPlaylist1>();
        // 1. build the query
        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUM_TABLE + " WHERE "
                + KEY_ALBUM_ID + " = " + album_id;
        Log.e("get Song qery::",selectQuery);
        //String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUM_TABLE;
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        WISPlaylist1 wisPlaylist = null;
        if (cursor.moveToFirst()) {
            do {

                wisPlaylist = new WISPlaylist1();
                wisPlaylist.setId(Integer.parseInt(cursor.getString(0)));
                wisPlaylist.setAlbum_id(Integer.parseInt(cursor.getString(1)));
                wisPlaylist.setThumbnail(cursor.getString(2));
                wisPlaylist.setName(cursor.getString(3));
                wisPlaylist.setArtists(cursor.getString(4));
                wisPlaylist.setCreated_at(cursor.getString(5));
                wisPlaylist.setAudio_path(cursor.getString(6));
                wisPlaylist.setDuration(cursor.getString(7));
                wisPlaylist.setType(cursor.getString(8));


                // Adding contact to list
                getSongs.add(wisPlaylist);
            } while (cursor.moveToNext());
        }
        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }
    public List<WISPlaylist1> getPlaylistAlbumss() {
        List<WISPlaylist1> getSongs = new ArrayList<WISPlaylist1>();
        // 1. build the query
//        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUM_TABLE + " WHERE "
//                + KEY_ALBUM_ID + " = " + album_id;
        String selectQuery = "SELECT  * FROM " + PLAYLIST_ALBUM_TABLE;
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // 3. go over each row, build music and add it to list
        // looping through all rows and adding to list
        WISPlaylist1 wisPlaylist = null;
        if (cursor.moveToFirst()) {
            do {

                wisPlaylist = new WISPlaylist1();
                wisPlaylist.setId(Integer.parseInt(cursor.getString(0)));
                wisPlaylist.setAlbum_id(Integer.parseInt(cursor.getString(1)));
                wisPlaylist.setThumbnail(cursor.getString(2));
                wisPlaylist.setName(cursor.getString(3));
                wisPlaylist.setArtists(cursor.getString(4));
                wisPlaylist.setCreated_at(cursor.getString(5));
                wisPlaylist.setAudio_path(cursor.getString(6));
                wisPlaylist.setDuration(cursor.getString(7));
                wisPlaylist.setType(cursor.getString(8));


                // Adding contact to list
                getSongs.add(wisPlaylist);
            } while (cursor.moveToNext());
        }
        Log.d("getSongs()", getSongs.toString());

        // return contact list
        return getSongs;
    }

    // Deleting single book
    public void deleteAllRecords() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database
        db.execSQL("DELETE FROM " + ALBUM_FILES_TABLE);
        db.execSQL("DELETE FROM " + SONGS_TABLE);
        db.execSQL("DELETE FROM " + SONGS_FILES_TABLE);
//

        // playlist song data

        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_TABLE);

        // PLAYLIST ALBUM DATA

        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUMS);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_TABLE);

        File dir = new File(Environment.getExternalStorageDirectory()+"wismusic");
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }

        db.close();

    }
    public void deleteAlumbs() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database
        db.execSQL("DELETE FROM " + ALBUM_FILES_TABLE);
        db.execSQL("DELETE FROM " + SONGS_TABLE);


        db.close();

    }
    public void deleteSongs() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database
        db.execSQL("DELETE FROM " + SONGS_FILES_TABLE);

        db.close();

    }
    public void deleteAllPlaylist() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database


        // playlist song data

        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_TABLE);

        // PLAYLIST ALBUM DATA

        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUMS);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_TABLE);
        db.close();

    }
    public void deleteAllPlaylistSongFiles() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database


        // playlist song data

        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_SONGS_TABLE);

        // PLAYLIST ALBUM DATA
        db.close();

    }
    public void deleteAllPlaylistAlbumFiles() {

        Log.d("clear","dataClear");
        SQLiteDatabase db = this.getWritableDatabase(); //get database


        // playlist song data

        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_FILES_TABLE);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUMS);
        db.execSQL("DELETE FROM " + PLAYLIST_ALBUM_TABLE);

        // PLAYLIST ALBUM DATA
        db.close();

    }

    public void deleteSongId(int song_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SONGS_FILES_TABLE, KEY_SONG_ID + " = ?",
                new String[] { String.valueOf(song_id) });

        Log.i("deleteResponse","songID");
    }
    public void deleteAlbumId(int album_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ALBUM_FILES_TABLE, KEY_ALBUM_ID + " = ?",
                new String[] { String.valueOf(album_id) });

        Log.i("deleteResponse","albumID");
    }
    public void deletePlaylistSongId(int playlist_song_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PLAYLIST_SONGS_FILES_TABLE, KEY_ID + " = ?",
                new String[] { String.valueOf(playlist_song_id) });

        Log.i("deleteResponse","playlistSong");
    }
    public void deletePlaylistAlbumId(int playlist_album_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PLAYLIST_ALBUM_FILES_TABLE, KEY_PLAYLIST_ID + " = ?",
                new String[] { String.valueOf(playlist_album_id) });

        Log.i("deleteResponse","playlistAlbum");
    }

    public int updateSongsPlaylist(WISPlaylistSongs wisPlaylistSongs) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, wisPlaylistSongs.getName());
        values.put(KEY_CREATED_AT, wisPlaylistSongs.getCreated_at());

        // updating row
        return db.update(PLAYLIST_SONGS_FILES_TABLE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(wisPlaylistSongs.getId()) });

    }
    public int updateAlbumPlaylist(WISPlaylistAlbum wisPlaylistSongs) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, wisPlaylistSongs.getName());
        values.put(KEY_CREATED_AT, wisPlaylistSongs.getCreated_at());

        // updating row
        return db.update(PLAYLIST_ALBUM_FILES_TABLE, values, KEY_PLAYLIST_ID + " = ?",
                new String[] { String.valueOf(wisPlaylistSongs.getId()) });
    }

}
