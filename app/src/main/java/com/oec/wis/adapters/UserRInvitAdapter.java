package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRInvitAdapter extends BaseAdapter {
    Context context;
    List<WISUser> data;

    public UserRInvitAdapter(Context context, List<WISUser> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_contact_rinvit, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPhoto.setImageResource(0);
        holder.tvName.setText(data.get(position).getFirstName());
        holder.tvCFriend.setText(String.valueOf(data.get(position).getnCFriend()));
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.get(position).getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPhoto.setImageResource(R.drawable.profile);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPhoto.setImageResource(R.drawable.profile);
                }
            }
        });
        holder.bInvit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAccept(holder.progress, position);
            }
        });
        holder.bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestCancel(holder.progress, position);
            }
        });
        holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ProfileView.class);
                i.putExtra("id", data.get(position).getId());
                context.startActivity(i);
            }
        });
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void requestAccept(final ProgressBar progress, final int position) {
        progress.setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_lien_am", data.get(position).getlInvit());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqInvit = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.acceptinvit_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                data.remove(position);
                                notifyDataSetChanged();
                            }
                            progress.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            progress.setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqInvit);
    }

    private void requestCancel(final ProgressBar progress, final int position) {
        progress.setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_lien_am", data.get(position).getlInvit());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqInvit = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.cancelinvit_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                data.remove(position);
                                notifyDataSetChanged();
                            }
                            progress.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            progress.setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqInvit);
    }

    private static class ViewHolder {
        TextView tvName, tvCFriend;
        CircularImageView ivPhoto;
        Button bInvit, bCancel;
        ProgressBar progress;

        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvCFriend = (TextView) view.findViewById(R.id.tvNCFriend);
            ivPhoto = (CircularImageView) view.findViewById(R.id.ivPhoto);
            bInvit = (Button) view.findViewById(R.id.bInvit);
            bCancel = (Button) view.findViewById(R.id.bCancel);
            progress = (ProgressBar) view.findViewById(R.id.loading);
            view.setTag(this);
        }
    }
}
