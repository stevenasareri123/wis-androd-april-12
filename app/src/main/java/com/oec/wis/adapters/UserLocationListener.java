package com.oec.wis.adapters;

/**
 * Created by asareri08 on 05/10/16.
 */
public interface UserLocationListener {

    public void updateUserLocationInfo(String formatted_address);


}
