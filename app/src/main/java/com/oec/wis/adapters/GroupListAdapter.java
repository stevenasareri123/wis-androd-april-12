package com.oec.wis.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.entities.Item;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconTextView;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by asareri08 on 26/07/16.
 */
public class GroupListAdapter extends BaseAdapter implements Filterable {

    List<WISChat> data;
    Context context;
    private List<WISChat>filteredData = null;
    List<WISAds> ads;

    private ItemFilter Filter = new ItemFilter();

    public GroupListAdapter(Context context, List<WISChat> data) {

        this.data = data;
        this.context = context;
        this.filteredData = data;

    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return filteredData.get(position).getGroupId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.group_row_items, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));

//        if (position % 2 == 0) {
//            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
////            holder.lastMsg.setTextColor(context.getResources().getColor(R.color.white));
//            holder.members.setTextColor(context.getResources().getColor(R.color.white));
//            holder.lastMsgDate.setTextColor(context.getResources().getColor(R.color.white));
//            holder.time.setTextColor(context.getResources().getColor(R.color.white));
//        }else{
//            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
////            holder.lastMsg.setTextColor(context.getResources().getColor(R.color.gray3));
//            holder.members.setTextColor(context.getResources().getColor(R.color.gray3));
//            holder.lastMsgDate.setTextColor(context.getResources().getColor(R.color.gray3));
//        }
        holder.groupIcon.setImageResource(0);

        holder.groupName.setText(filteredData.get(position).getGroupName());
        holder.members.setText(String.valueOf(filteredData.get(position).getMembers())+"  Friends");

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
        holder.members.setTypeface(font);
        holder.groupName.setTypeface(font);
        holder.time.setTypeface(font);
        holder.lastMsgDate.setTypeface(font);
//        holder.lastMsg.setText(filteredData.get(position).getMsg());

        holder.notifyPub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPubs(filteredData.get(position).getGroupId());
            }
        });

        if(filteredData.get(position).getDateTime().equals("")){

           System.out.println("no history");

            holder.lastMsgDate.setText("");
            holder.time.setText("");


        }else {


            Calendar c = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat format_two = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat hour = new SimpleDateFormat("HH");
            SimpleDateFormat min = new SimpleDateFormat("mm");
            SimpleDateFormat sec = new SimpleDateFormat("ss");
            Date d = null;
            Log.d("date", filteredData.get(position).getDateTime());
            String created_date = "",h="";
//            String created_date = "",h="",m="",s="";

            try {
                d = format.parse(filteredData.get(position).getDateTime());
                Date newDate = d;
                created_date = format_two.format(newDate);
                h = hour.format(newDate);
//                m=min.format(newDate);
//                s=sec.format(newDate);
                c.setTime(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.lastMsgDate.setText(created_date);
            Log.d("created date",created_date);
            Date startDate =d;
            Date endDate = currentUtcDateTime();
//            printDifference(startDate,endDate);
//
//
            String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();

//            holder.lastMsgDate.setText(created_date);

            if (rDate != null) {
                String text = h.concat("h");

//                String text = h.concat("H").concat(m).concat("'").concat(s);
                holder.time.setText(text);
//                if (net.danlew.android.joda.DateUtils.isToday(new DateTime(d))) {
//
////                Date endDate = currentUtcDateTime();
////                Date startDate =d;
//
////                    long days = diff / (24 * 60 * 60 * 1000);
//
//
////                    holder.time.setText(printDifference(startDate,endDate));
//
//                } else {
//                    holder.time.setText(" ");
//                }

            }
        }
        loadImage(holder, filteredData.get(position).getGroupIcon());









        return convertView;
    }

    public void loadImage(final ViewHolder holder, String path) {
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + path, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.groupIcon.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.groupIcon.setImageBitmap(response.getBitmap());
                } else {
                    holder.groupIcon.setImageResource(R.drawable.empty);
                }
            }
        });

    }

    public String getUserFormatTimeZoneDate(String created_at){

//        override date and time
        Date date=new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);
        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

        TimeZone tz = TimeZone.getTimeZone(Tools.getData(context,"timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String userTimeZoneFormateddate = destFormat.format(date);
        Log.i("resilt timezone ",userTimeZoneFormateddate);


        String differenceTime= String.valueOf(DateUtils.getRelativeTimeSpanString(getDateInMillis(created_at),getDateInMillis(userTimeZoneFormateddate), 0));


        return differenceTime;
    }
    public static long getDateInMillis(String srcDate) {

        Log.i("src date",srcDate);
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return 0;
    }


    public Date currentUtcDateTime(){
        Date date=new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);


        TimeZone tz = TimeZone.getTimeZone(Tools.getData(context,"timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);


        try {
            return destFormat.parse(result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String printDifference(Date startDate, Date endDate){




        System.out.println(startDate);
        System.out.println(endDate);


        Interval interval =
                new Interval(startDate.getTime(), endDate.getTime());
        Period period = interval.toPeriod();

        String hours= String.valueOf(period.getHours());
        String minutes = String.valueOf(period.getMinutes());
        String seconds = String.valueOf( period.getSeconds());

        String elapsedTime = hours.concat("h").concat(minutes).concat("'").concat(seconds);

        System.out.printf(
                "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
                period.getYears(), period.getMonths(), period.getDays(),
                period.getHours(), period.getMinutes(), period.getSeconds());

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedTime;

    }
    private void dialogPubs(int idFriend) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pickpub);
        ListView lvPubs = (ListView) dialog.findViewById(R.id.lvPubs);
        TextView headerTitleTV =(TextView)dialog.findViewById(R.id.headerTitleTV);
        Button bFinish =(Button)dialog.findViewById(R.id.bFinish);


        try{

            Typeface font =Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            headerTitleTV.setTypeface(font);
            bFinish.setTypeface(font);

        }catch (NullPointerException ex ){

        }

        dialog.findViewById(R.id.bFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("pub dialog","dismissed");
                dialog.dismiss();
            }
        });
        dialog.show();
        loadMyAds(dialog, lvPubs, idFriend);
    }
    private void loadMyAds(final Dialog dialog, final ListView lvPubs, final int idFriend) {
        dialog.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        ads = new ArrayList<>();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.actpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("pub response", String.valueOf(response));

                        try {
                            if (response.getString("result").equals("true")) {
                                Log.i("pub response", String.valueOf(response));
                                JSONArray data = response.getJSONArray("data");

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");

                                        } catch (Exception e) {
                                        }

                                        ads.add(new WISAds(obj.getInt("idpub"), obj.getInt("idact"), obj.getString("titre"), obj.getString("description"), obj.getString("photo"), obj.getString("type_obj"), thumb,true));
                                        Log.i("pub adapter count", String.valueOf(ads.size()));
                                        lvPubs.setAdapter(new PickPubAdapter(context, ads, idFriend));

                                    } catch (Exception e) {

                                    }
                                }
                            }
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }

    @Override
    public Filter getFilter() {
        return Filter;
    }

    private static class ViewHolder {
        TextView groupName, lastMsgDate, members,time;
        EmojiconTextView lastMsg;
        ImageView groupIcon,notifyPub,bCall;
        LinearLayout llContent;


        public ViewHolder(View view) {
            groupName = (TextView) view.findViewById(R.id.groupName);
//            lastMsg = (EmojiconTextView) view.findViewById(R.id.lastMessage);
            //tvLastMsg.setUseSystemDefault(true);
            lastMsgDate = (TextView) view.findViewById(R.id.tvDate);
            groupIcon = (ImageView) view.findViewById(R.id.ivPic);
            members = (TextView) view.findViewById(R.id.members);
            time = (TextView)view.findViewById(R.id.time);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
            notifyPub = (ImageView)view.findViewById(R.id.bNotif);
            bCall = (ImageView)view.findViewById(R.id.bCall);

            view.setTag(this);
        }
    }

    // Filter Class
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<WISChat> tempList = new ArrayList<WISChat>();

                // search content in friend list
                for (WISChat user : data) {
                    if (user.getGroupName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = data.size();
                filterResults.values = data;
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<WISChat>) results.values;
            notifyDataSetChanged();
        }

    }
}
