package com.oec.wis.adapters;

/**
 * Created by asareri12 on 02/02/17.
 */

public interface RefreshListener {

    public void refreshData();

    //public void refreshWithType(String type, WISAlbum album, WISMusic songs);
}
