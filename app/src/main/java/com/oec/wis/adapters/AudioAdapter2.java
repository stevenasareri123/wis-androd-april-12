package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISMusic;

import java.util.List;

public class AudioAdapter2 extends BaseAdapter {



    Context context;
    List<WISMusic> data;
    String type;
    GalleryListener galleryListener;

    public AudioAdapter2(Context context, List<WISMusic> data, String type, GalleryListener listener) {
        this.context = context;
        this.data = data;
        this.type=type;
        this.galleryListener  =  listener;

    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.song_layout, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText("Titre :"+data.get(position).getName());
        holder.artists.setText("Artiste :"+data.get(position).getArtists());
        //holder.imageView.setImageResource(R.drawable.empty);
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.imageView.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.imageView.setImageBitmap(response.getBitmap());
                } else {
                    holder.imageView.setImageResource(R.drawable.empty);
                }
            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap image=((BitmapDrawable)holder.imageView.getDrawable()).getBitmap();

                galleryListener.pickedItems(String.valueOf(data.get(position).getId()),image,"audio", String.valueOf(data.get(position).getPath()));

            }
        });



        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {

        TextView name,artists;
        ImageView imageView;


        public ViewHolder(View view) {

            name = (TextView) view.findViewById(R.id.name);
            artists=(TextView) view.findViewById(R.id.artists);
            imageView = (ImageView) view.findViewById(R.id.ivMusic);

            view.setTag(this);
        }
    }
}
