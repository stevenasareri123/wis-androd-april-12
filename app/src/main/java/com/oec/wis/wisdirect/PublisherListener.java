package com.oec.wis.wisdirect;

import com.oec.wis.entities.WISCmt;

import java.util.List;

/**
 * Created by asareri12 on 14/03/17.
 */

public interface PublisherListener {

    public void postComments(String message);
    public void onControlsClicked();

    public void updateViewCount(String viewCount);
}
