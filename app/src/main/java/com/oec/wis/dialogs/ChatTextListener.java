package com.oec.wis.dialogs;

import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

/**
 * Created by asareri-07 on 3/2/2017.
 */
public interface ChatTextListener {

    public void didReceiveTextMessage(PNMessageResult messageResult);

    public void onTextPublishSuccess(PNStatus status);

    public void didTextReceivePresenceEvent(PNPresenceEventResult eventResult);
}
