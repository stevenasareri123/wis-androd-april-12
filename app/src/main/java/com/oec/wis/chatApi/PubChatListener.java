package com.oec.wis.chatApi;

import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

/**
 * Created by asareri08 on 03/04/17.
 */

public interface PubChatListener {

    public void didReceiveTextMessage(PNMessageResult messageResult);

    public void onTextPublishSuccess(PNStatus status);

    public void didTextReceivePresenceEvent(PNPresenceEventResult eventResult);
}
