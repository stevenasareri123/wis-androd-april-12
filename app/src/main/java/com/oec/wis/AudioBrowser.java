package com.oec.wis;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.oec.wis.entities.UserNotification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asareri12 on 07/12/16.
 */

public class AudioBrowser extends ListActivity {


    public static int STATE_SELECT_ALBUM=0;
    public static int STATE_SELECT_SONG=0;
    int current_state = STATE_SELECT_ALBUM;
    Cursor cursor;
    long ALBUM_ID;

    ListView listView;
    GridView gridView;
    List<String> mylist = new ArrayList<String>();
    List<String> mylistids = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_music);

        listView = (ListView) findViewById(R.id.list);

        String[] columns = new String[] { MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM,};
        Cursor cursor = managedQuery(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, columns, null, null, null);

        if(cursor!=null)
        {
            while (cursor.moveToNext())
            {

                ALBUM_ID = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Albums._ID));
                Log.v("output1", String.valueOf(ALBUM_ID));

                mylist.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM)));
                mylistids.add(String.valueOf(ALBUM_ID));

            }
        }

        new UserNotification().setIds(mylistids);
        new UserNotification().setNames(mylist);
        Log.v("files", String.valueOf(mylist));
        Log.v("filesids", String.valueOf(mylistids));

        ListAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, new String[] {MediaStore.Audio.Albums.ALBUM},
                new int[] {android.R.id.text1});
        setListAdapter(adapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent i = new Intent(this,SelectAll.class);
        i.putExtra("album_id", String.valueOf(id));
        Log.i("album_id", String.valueOf(id));
        startActivity(i);
        onBackPressed();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
