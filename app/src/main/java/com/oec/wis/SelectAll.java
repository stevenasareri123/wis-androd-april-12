package com.oec.wis;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.oec.wis.database.DatabaseHandler;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.tools.ProgressHttpEntityWrapper;
import com.oec.wis.tools.Tools;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by asareri12 on 12/11/16.
 */

public class SelectAll extends Activity {
    private int count;
    private boolean[] thumbnailsselection;
    private String[] arrPath;
    private String[] nameFile;
    private ImageAdapter imageAdapter;
    String album_name;
    String album_artists;
    String albumIcon;
    String durationStr1;
    String ALBUM_ID;
    List<String> selectImages;
    List<String> songNames;
    List<String> artistnames;
    List<String> songdurations;
    List<String> songIcons;

    String songlist;
    String artistlist;
    String durationlist;

    List<WISAlbum> album;

    ProgressBar progressBar;
    File imageFile;
    ListView listView;

    DatabaseHandler db;

    List<String> mylist = new ArrayList<String>();
    List<String> mylistids = new ArrayList<String>();

    private Handler handler = new Handler();
    private int progress = 0;
    private int secondaryProgress = 0;

    int progressValue=0;
    ProgressDialog mProgressDialog;

    List<String> CurrentAudioPath = new ArrayList<>();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_songs);

        mProgressDialog = new ProgressDialog(this);
        db = new DatabaseHandler(this);


        Intent intent = getIntent();
        ALBUM_ID = intent.getStringExtra("album_id");

        mylist =new UserNotification().getNames();
        mylistids = new UserNotification().getIds();

        String[] column = { MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.MIME_TYPE,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ARTIST
        };

        String where = android.provider.MediaStore.Audio.Media.ALBUM + "=?";

        int retval=mylistids.indexOf(String.valueOf(ALBUM_ID));

        String alb =mylist.get(retval);

        String whereVal[] = { alb };

        String orderBy = MediaStore.Audio.Media._ID;

        Cursor imagecursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                column, where, whereVal, orderBy);

        int image_column_index = imagecursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
        this.count = imagecursor.getCount();

        this.arrPath = new String[this.count];
        this.nameFile = new String[this.count];
        this.thumbnailsselection = new boolean[this.count];
        for (int i = 0; i < this.count; i++) {
            imagecursor.moveToPosition(i);
            int id = imagecursor.getInt(image_column_index);
            int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            arrPath[i] = imagecursor.getString(dataColumnIndex);
            nameFile[i] = arrPath[i].substring(arrPath[i].lastIndexOf("/") + 1, arrPath[i].length());


            album_name = imagecursor.getString(imagecursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
            Log.i("albumname", album_name);
            album_artists = imagecursor.getString(imagecursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            Log.i("albumartists", album_artists);
            long duration = (imagecursor.getInt(imagecursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)));;
            long durationSec = TimeUnit.MILLISECONDS.toSeconds(duration);
            long minutes,seconds,hours;
            seconds = durationSec % 60;
            durationSec/= 60;
            minutes = durationSec % 60;
            durationSec /= 60;
            hours = durationSec % 24;
            durationStr1= String.valueOf(hours)+":"+ String.valueOf(minutes)+":"+ String.valueOf(seconds);

        }
        Log.d("soundPAth1", String.valueOf(arrPath));
        ListView imagegrid = (ListView) findViewById(R.id.list);
        imageAdapter = new ImageAdapter();
        imagegrid.setAdapter(imageAdapter);
        //imagecursor.close();

        final Button selectBtn = (Button) findViewById(R.id.selectBtn);
        selectBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                // TODO Auto-generated method stub
                final int len = thumbnailsselection.length;
                int cnt = 0;

                selectImages = new ArrayList<String>();
                songNames = new ArrayList<String>();
                songdurations = new ArrayList<String>();
                artistnames = new ArrayList<String>();
                songIcons = new ArrayList<String>();

                MediaMetadataRetriever metaRetriver;

                for (int i = 0; i < len; i++) {
                    if (thumbnailsselection[i]) {
                        cnt++;
                        selectImages.add(arrPath[i]);
                        songNames.add(nameFile[i]);

                        songlist = TextUtils.join("|| ", songNames);

//                            Log.i("Namess",artistlist);

                        //songlist = songNames.toString().replace("[", "").replace("]", "").trim();

                        //songNames.toString().replaceAll("[\\[\\]\\ ]", "");


                        metaRetriver = new MediaMetadataRetriever();

                        try {
                            Log.e("ArrayPath",arrPath[i]);
                            metaRetriver.setDataSource(arrPath[i]);
                            byte[] art = metaRetriver.getEmbeddedPicture();

                            Log.e("Art Icon", String.valueOf(art));
                            if(art!=null)
                            {
                                Bitmap songImage1 = BitmapFactory.decodeByteArray(art, 0, art.length);
                                //persistImage(songImage1,"wis_music");

                                String PATH = Environment.getExternalStorageDirectory()
                                        + "/wismusic/";
                                File file = new File(PATH);
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                Random generator = new Random();
                                int n = 10000;
                                n = generator.nextInt(n);
                                String fname = "wis_music-"+ n + ".jpg";
                                imageFile = new File(PATH, fname);

                                OutputStream os;
                                try {
                                    os = new FileOutputStream(imageFile);
                                    songImage1.compress(Bitmap.CompressFormat.JPEG, 100, os);
                                    os.flush();
                                    os.close();
                                } catch (IOException e) {

                                    System.out.println("Exception=====>" +e.getMessage());

                                }

                                Log.i("imageIcon", String.valueOf(imageFile));
                            }
                            else {
//                                    Bitmap songImage1 = BitmapFactory.decodeByteArray(art, 0, art.length);
//                                    persistImage(songImage1,"wis_music");

                                Bitmap songImage1 = BitmapFactory.decodeResource( getResources(), R.drawable.empty);

                                String PATH = Environment.getExternalStorageDirectory()
                                        + "/wismusic/";
                                File file = new File(PATH);
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                Random generator = new Random();
                                int n = 10000;
                                n = generator.nextInt(n);
                                String fname = "wis_music-"+ n + ".jpg";
                                imageFile = new File(PATH, fname);

                                OutputStream os;
                                try {
                                    os = new FileOutputStream(imageFile);
                                    songImage1.compress(Bitmap.CompressFormat.JPEG, 100, os);
                                    os.flush();
                                    os.close();
                                } catch (Exception e) {

                                }



                                Log.i("imageNull", String.valueOf(imageFile));
                            }


                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                        songIcons.add(String.valueOf(imageFile));


                        if(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)!=null)
                        {
                            artistnames.add(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                        }
                        else {
                            artistnames.add("wismusic");
                        }




//                            Collections.replaceAll(artistnames, "null", "wismusic");

                        artistlist = TextUtils.join("|| ", artistnames);


                        //Log.i("Namess",artistlist);

                        //artistlist=artistnames.toString().replace("[", "").replace("]", "").trim();
                        //artistnames.toString().replaceAll("[\\[\\]\\ ]", "");

                        try {
                            String durationtime = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

                            if(durationtime!=null)
                            {
                                long duration = Long.parseLong(durationtime);
                                long durationSec = TimeUnit.MILLISECONDS.toSeconds(duration);
                                long minutes,seconds,hours;
                                seconds = durationSec % 60;
                                durationSec/= 60;
                                minutes = durationSec % 60;
                                durationSec /= 60;
                                hours = durationSec % 24;
                                String durationStr= String.valueOf(hours)+":"+ String.valueOf(minutes)+":"+ String.valueOf(seconds);
                                //Log.i("time",durationStr);
                                songdurations.add(durationStr);

                            }
                            else {
                                songdurations.add("00:00:04");
                            }
                            //Log.i("duk",durationtime);

                            durationlist = TextUtils.join("|| ", songdurations);

                        }
                        catch (NullPointerException e)
                        {

                        }



                        //durationlist=songdurations.toString().replace("[", "").replace("]", "").trim();

                    }
                }
                if (cnt == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Please select at least one song",
                            Toast.LENGTH_LONG).show();
                } else {


                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        Log.i("Calling","executeOnExecutor()");
                        new DoUpload().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(selectImages), String.valueOf(songIcons),album_name, album_artists, durationStr1, "album",songlist,artistlist,durationlist);
                    }
                    else {
                        Log.i("Calling","execute()");
                        new DoUpload().execute(String.valueOf(selectImages), String.valueOf(songIcons),album_name, album_artists, durationStr1, "album",songlist,artistlist,durationlist);
                    }
                }
            }
        });
    }



    public void testUpload(String fPath, String songIcon, final String songName, final String artist, final String songDuration, final String type, final String songnames, final String artists, final String durations){

        Log.e("TEST","UPLOAD CALING");

        new Thread(new Runnable(){
            @Override
            public void run() {
                // Do network action in this function



                JSONObject jsonObject = Tools.uploadMusicToServer(selectImages,songIcons,songName,artist,songDuration,type,songnames,artists,durations,getString(R.string.server_url) + getString(R.string.upload_music_meth), Tools.getData(getApplicationContext(), "idprofile"), Tools.getData(getApplicationContext(), "token"));

                Log.e("RESPONSE_DATA", String.valueOf(jsonObject));
                runOnUiThread(new Runnable() {
                    //                    @Override
                    public void run() {

                        mProgressDialog.dismiss();
                    }
                });
            }
        }).start();

    }

    public class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("ASYNC", "ASYNC");
            mProgressDialog.setTitle("Please Wait");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (!result.equals("")) {
                try {
                    JSONObject response = new JSONObject(result);
                    Log.i("response",result);
                    //Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();
                    if (response.getBoolean("result")) {
                        progressValue=0;
                        mProgressDialog.dismiss();
                        final JSONArray data = response.getJSONArray("album_files");
                        for (int i = 0; i < data.length(); i++) {
                            try {
                                JSONObject obj = data.getJSONObject(i);
                                JSONArray songsArry = obj.getJSONArray("songs");
                                Log.i("songArray", String.valueOf(songsArry));
                                List<WISSongs> arrayList = new ArrayList<>();

                                List<String> pathArray = new ArrayList<>();


                                db.addAlbums(new WISAlbum(obj.getInt("album_id"),obj.getString("album_name"),obj.getString("album_artists"),obj.getString("album_created_at")));

                                int albumid = obj.getInt("album_id");

                                for(int c =0;c<songsArry.length();c++)
                                {
                                    JSONObject obj1 = songsArry.getJSONObject(c);
                                    arrayList.add(new WISSongs(obj1.getInt("id"),obj1.getString("name"),obj1.getString("artists"),obj1.getString("duration"),obj1.getString("audio_path"),obj1.getString("audio_thumbnail"),obj1.getString("created_at")));
                                    db.putSongs(new WISSongs(obj1.getInt("id"),obj1.getString("name"),obj1.getString("artists"),obj1.getString("duration"),obj1.getString("audio_path"),obj1.getString("audio_thumbnail"),obj1.getString("created_at"),albumid));

                                    Log.i("files",obj1.getString("audio_path"));

                                    Log.i("selectedImage",selectImages.get(c));

                                    saveAudioToLocalPath(obj1.getString("audio_path"),selectImages.get(c));

                                }

                                album.add(new WISAlbum(obj.getString("album_name"),obj.getInt("album_id"),obj.getString("album_artists"),obj.getString("album_created_at"),arrayList));


                            } catch (Exception e) {
                            }
                        }



                        Toast.makeText(getApplicationContext(),"album songs are uploaded", Toast.LENGTH_LONG).show();
                        UserNotification.setIsAlbumUploaded(Boolean.TRUE);
                        finish();




                    }else {
                        //Toast.makeText(getApplicationContext(),"audio extension not supported", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                        UserNotification.setIsAlbumUploaded(Boolean.FALSE);
                    }
                } catch (JSONException e) {
                }

            } else {
//                Toast.makeText(getApplicationContext(),"audio extension not supported", Toast.LENGTH_LONG).show();
//                mProgressDialog.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... urls) {

            Log.e("calling","uploadMusic");

            Log.i("filePath",urls[0]);
            Log.i("songIcon",urls[1]);
            Log.i("songName",urls[2]);
            Log.i("artist",urls[3]);
            Log.i("songDuration",urls[4]);
            Log.i("type",urls[5]);
            Log.i("songnames",urls[6]);
            Log.i("songartists",urls[7]);
            Log.i("durations",urls[8]);


            try {

                //return  Tools.uploadMusic(selectImages,songIcons,urls[2],urls[3],urls[4],urls[5],urls[6],urls[7],urls[8],getString(R.string.server_url) + getString(R.string.upload_music_meth),Tools.getData(getApplicationContext(), "idprofile"), Tools.getData(getApplicationContext(), "token"));
                return uploadMusic(urls[0],urls[1],urls[2],urls[3],urls[4],urls[5],urls[6],urls[7],urls[8],getString(R.string.server_url) + getString(R.string.upload_music_meth), Tools.getData(getApplicationContext(), "idprofile"), Tools.getData(getApplicationContext(), "token"));
            } catch (Exception e) {
                System.out.println("=====EXCEPTION=====");
                Log.e("EXCEPTION=>",e.getLocalizedMessage());

                if(mProgressDialog!=null)
                {
                    mProgressDialog.dismiss();
                }

                Toast.makeText(getApplicationContext(),"Connection TimeOut", Toast.LENGTH_SHORT);

                return "";
            }

        }
    }

    public void saveAudioToLocalPath(String fname, String sourcefile){

        if(Tools.isStorageAvailable()){

            System.out.println("Save AudioPath=>");

            Log.e("Audio path", sourcefile);
            Log.e("Audio path1",fname);

            File localAudioFile = new File(sourcefile);

            try {


                String sep = File.separator; // Use this instead of hardcoding the "/"
                String newFolder = "wismusic";
                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                File myNewFolder = new File(extStorageDirectory + sep + newFolder);
                if(!myNewFolder.exists()){

                    myNewFolder.mkdir();
                }

                String mediaFile = Environment.getExternalStorageDirectory().toString() + sep + newFolder + sep + fname;

                Log.e("Local media file path",mediaFile);
                InputStream in = new FileInputStream(localAudioFile);
                File file = new File(mediaFile);
                OutputStream out = new FileOutputStream(file);

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);

                    try {
                        CurrentAudioPath.remove(sourcefile);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }
                in.close();
                out.close();

//                    }
//                }
            } catch (Exception e) {}




        }
        else{

            Log.e("Space not available","***");
        }

    }


    private class DownloadFile extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            int count;
            String imageURL = url[0];
            try {
                URL urls = new URL(imageURL);

                URLConnection conexion = urls.openConnection();

                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(urls.openStream());
                imageURL = imageURL.replace(getString(R.string.server_url3),"");
                String fname = imageURL;
                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                //Log.v("log_tag", "PATH: " + PATH);
                File file = new File(PATH);
                if (!file.exists()) {
                    file.mkdirs();
                }
                File outputFile = new File(file, fname);
                if (!outputFile.exists ())
                {
                    try {
                        OutputStream output = new FileOutputStream(outputFile);
                        byte data[] = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {

                            total += count;
                            publishProgress((int) (total * 100 / lenghtOfFile));

                            output.write(data, 0, count);

                        }
                        output.flush();

                        output.close();

                        input.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //outputFile.delete ();


            } catch (Exception e) {
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
    public String uploadMusic(String f, String t, String songName, String artist, String songDuration, String type, String songnames, String artists, String durations, String url, String userId, String token) throws ParseException, IOException {

        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(url);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);



        ProgressHttpEntityWrapper.ProgressCallback progressCallback = new ProgressHttpEntityWrapper.ProgressCallback() {

            @Override
            public void progress(float progress) {
                //Use the progress


                progressValue = (int) Math.round(progress);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //mProgressDialog.setMessage(String.valueOf(progressValue).concat(" %"));
                        mProgressDialog.setMessage("Uploading"+"\n"+ String.valueOf(progressValue).concat(" %"));
                    }
                });
                //Log.i("progress",String.valueOf(progressValue));
                //setProgress(String.valueOf(progressValue));

            }

        };



        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());

        for(int i=0;i<selectImages.size();i++) {

//            CurrentAudioPath.clear();
//            CurrentAudioPath.add(selectImages.get(i));

            FileBody fileBody = new FileBody(new File(selectImages.get(i)));
            builder.addPart("file".concat(String.valueOf(i)), fileBody);
            Log.i("file name","file".concat(String.valueOf(i)));
        }

        Log.e("Song icons", String.valueOf(songIcons));

        for(int i=0;i<songIcons.size();i++) {


            FileBody fileBody = new FileBody(new File(songIcons.get(i)));
            builder.addPart("audio_icons".concat(String.valueOf(i)), fileBody);


            Log.i("audio_icons","audio_icons".concat(String.valueOf(i)));
        }
        builder.addTextBody("user_id", userId);
        builder.addTextBody("album_name", songName);
        builder.addTextBody("album_artists", artist);
        builder.addTextBody("type", type);
        builder.addTextBody("album_duration", songDuration);
        builder.addTextBody("name",songnames);
        builder.addTextBody("artists",artists);
        builder.addTextBody("duration",durations);

        HttpEntity entity = builder.build();
        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
//        method.setEntity(entity);
        method.setEntity(new ProgressHttpEntityWrapper(entity, progressCallback));

        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";

        if(response!=null) {
            HttpEntity resEntity = response.getEntity();
            try {
                res = EntityUtils.toString(resEntity);
                Log.i("muliple response", String.valueOf(res));

                if (response.getEntity() != null) {
                    response.getEntity().consumeContent();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }
        return res;

    } // end of upl


    public class ImageAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ImageAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return count;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.songs_item, null);
                holder.checkbox = (CheckBox) convertView.findViewById(R.id.itemCheckBox);
                holder.txtview = (TextView) convertView.findViewById(R.id.itemtxtview);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkbox.setId(position);
            holder.txtview.setText(nameFile[position]);
            holder.checkbox.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    CheckBox cb = (CheckBox) v;
                    int id = cb.getId();
                    if (thumbnailsselection[id]){
                        cb.setChecked(false);
                        thumbnailsselection[id] = false;
                    } else {
                        cb.setChecked(true);
                        thumbnailsselection[id] = true;
                    }
                }
            });
            holder.checkbox.setChecked(thumbnailsselection[position]);
            holder.id = position;
            return convertView;
        }
    }
    class ViewHolder {
        CheckBox checkbox;
        TextView txtview;
        int id;
    }
}
